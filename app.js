const querystring = require("querystring");
const handleBlogRouter = require("./src/router/blog");
const handleUserRouter = require("./src/router/user");

// 用于处理postData
function getPostData(req) {
  return new Promise((resolve, reject) => {
    if (req.method !== "POST") {
      resolve({});
      return;
    }
    if (req.headers["content-type"] !== "application/json") {
      resolve({});
      return;
    }
    var result = "";
    req.on("data", chunk => {
      result += chunk;
    });
    req.on("end", () => {
      resolve(result);
    });
  });
}

const serverHander = (req, res) => {
  // 设置数据返回格式 JSON
  res.setHeader("content-type", "application/json");

  // 解析 path
  req.path = req.url.split("?")[0];

  // 解析query
  req.query = querystring.parse(req.url.split("?")[1]);

  getPostData(req).then(async postData => {
    req.body = postData;
    // console.log(postData);
    // 处理blog路由
    let blogData = await handleBlogRouter(req, res);
    if (blogData) {
      res.end(JSON.stringify(blogData));
      return;
    }

    // 处理user路由
    // let userData = handleUserRouter(req, res);
    // if (userData) {
    //   res.end(JSON.stringify(userData));
    //   return;
    // }

    // 未命中路由，返回404
    res.writeHead(404, { "Content-type": "text/plain" });
    res.write("404 Not Found \n");
    res.end();
  });
};

module.exports = serverHander;
