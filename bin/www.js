const http = require("http");
const serverHandle = require("../app.js");
const app = http.createServer(serverHandle);
const port = "8080";

app.listen(port, () => {
  console.log("server has started at http://localhost:" + port);
});
