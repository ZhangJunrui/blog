// 定义路由地址
const GET_BLOG_LIST = "/api/blog/list";
const GET_BLOG_DETAIL = "/api/blog/detail";
const BUILD_BLOG = "/api/blog/new";
const UPDATE_BLOG = "/api/blog/update";
const DELETE_BLOG = "/api/blog/delete";

// user

const USER_LOGIN = "/api/user/login";
module.exports = {
  GET_BLOG_LIST,
  GET_BLOG_DETAIL,
  BUILD_BLOG,
  UPDATE_BLOG,
  DELETE_BLOG,
  USER_LOGIN
};
