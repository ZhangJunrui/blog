const routes = require("./config");
const { loginCheck } = require("../controller/user.js");
const { SuccessModel, ErrorModel } = require("../model/resModel");

const handleUserRouter = (req, res) => {
  const method = req.method.toUpperCase();

  // 登录
  if (method === "POST" && req.path === routes.USER_LOGIN) {
    const { username, password } = req.body;
    console.log(username, password);
    const result = loginCheck(username, password);
    if (result) {
      return new SuccessModel();
    }
    return new ErrorModel("登陆失败");
  }
};

module.exports = handleUserRouter;
