const routes = require("./config");
const {
  getList,
  getDetail,
  newBlog,
  updateBlog,
  deleteBlog
} = require("../controller/blog");
const { SuccessModel, ErrorModel } = require("../model/resModel");

const handleBlogRouter = async (req, res) => {
  const { id } = req.query;
  const method = req.method.toUpperCase();
  // 获取博客列表
  if (method === "GET" && req.path === routes.GET_BLOG_LIST) {
    const { author, keyword } = req.query;
    try {
      const listData = await getList(author, keyword);
      if (listData) {
        return new SuccessModel(listData);
      }
    } catch (err) {
      return new ErrorModel(err);
    }
  }

  // 获取博客详情
  if (method === "GET" && req.path === routes.GET_BLOG_DETAIL) {
    let listData = getDetail(id);
    return new SuccessModel(listData);
  }

  // 新建一篇博客
  if (method === "POST" && req.path === routes.BUILD_BLOG) {
    const data = newBlog(req.body);
    return new SuccessModel(data);
  }

  // 更新博客
  if (method === "POST" && req.path === routes.UPDATE_BLOG) {
    const result = updateBlog(id, req.body);
    return result ? new SuccessModel(result) : new ErrorModel("更新博客失败");
  }

  // 删除博客
  if (method === "POST" && req.path === routes.DELETE_BLOG) {
    const result = deleteBlog(id);
    return result ? new SuccessModel(result) : new ErrorModel("删除博客失败");
  }
};

module.exports = handleBlogRouter;
