const mysql = require("mysql");
const MYSQL_CONF = require("../config/db");

// 创建 mysql 连接对象
const connection = mysql.createConnection(MYSQL_CONF);

// 开始连接
connection.connect();

// 统一执行 sql 语句
function exec(sql) {
  const promise = new Promise((resolve, reject) => {
    connection.query(sql, (err, result) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(result);
    });
  });
  return promise;
}

module.exports = {
  exec
};
