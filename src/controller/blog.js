const { exec } = require("../db/mysql.js");
const getList = (author, keyword) => {
  // 这里1=1是防止 autor 和 keyword 没有值的情况下，sql语句出错
  let sql = `select * from blog where 1=1`;
  if (author) {
    sql += ` and author=${author}`;
  }
  if (keyword) {
    sql += ` and title like '%${keyword}%'`;
  }
  sql += ` order by createtime desc;`;
  // 先返回假数据，（格式是正确的）
  return exec(sql);
};

const getDetail = id => {
  return [
    {
      id: 2222,
      title: "标题22",
      content: "内容2222",
      createTime: "1213132131232222",
      author: "张三"
    }
  ];
};

const newBlog = (blogData = {}) => {
  // blogData 是一个博客
  return {
    id: 3 // 表示新建博客，插入到数据表里面的 id
  };
};

const updateBlog = (id, blogData = {}) => {
  // id 是要更新博客的id
  console.log("update  blog", id, blogData);
  return true;
};

const deleteBlog = id => {
  console.log("删除博客id ", id);
  return true;
};
module.exports = {
  getList,
  getDetail,
  newBlog,
  updateBlog,
  deleteBlog
};
