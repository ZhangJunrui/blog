const env = process.env.NODE_ENV || 'dev';   // 环境变量

let MYSQL_CONF = {};

if (env === 'dev') {
    MYSQL_CONF = {
        host: 'localhost',
        user: 'root',
        password: 'root',
        prot: '3306',
        database: 'myblog'
    };
}

// 线上的信息
if (env ==='production') {
    MYSQL_CONF = {
        host: 'localhost',
        user: 'root',
        password: 'root',
        prot: '3306',
        database: 'myblog'
    };
}
module.exports = MYSQL_CONF;